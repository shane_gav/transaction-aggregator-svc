# Runnning
## Prep
Modify the application.properties file for the file repo destination or else override at command line.

## Command

```
./mvnw spring-boot:run
```


# Sample commands

## POST

```
curl --request POST \
  --url http://localhost:8080/transaction-aggregations \
  --header 'Content-Type: application/json' \
  --data '[
	{
		"date": "2018-01-01",
		"amount": 50.00,
		"type": "debit"
	},
		{
		"date": "2019-07-01",
		"amount": 50.00,
		"type": "debit"
	}
]'
```

## GET

```
curl --request GET \
  --url 'http://localhost:8080/transaction-aggregations?date=2019-07-01&type=debit'
```


## Insomnia collection
Included also is `InsomniaCollection.json` which you can import into insomnia rest client for testing.

