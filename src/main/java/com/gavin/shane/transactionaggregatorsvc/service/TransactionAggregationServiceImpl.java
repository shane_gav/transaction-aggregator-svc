package com.gavin.shane.transactionaggregatorsvc.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gavin.shane.transactionaggregatorsvc.dao.TransactionAggregationDAO;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransactionAggregationServiceImpl implements TransactionAggregationService{
    private static final Object writelock = new Object();
    private static Map<TransactionAggregation, BigDecimal> transactionAggregations;

    private TransactionAggregationDAO transactionAggregationDAOImpl;
    private TransactionAggregationUpdateLogService transactionAggregationUpdateLogServiceImpl;
    
    
    
    public TransactionAggregationServiceImpl(
        @Autowired TransactionAggregationDAO transactionAggregationDAOImpl,
        @Autowired TransactionAggregationUpdateLogService transactionAggregationUpdateLogServiceImpl){
        this.transactionAggregationDAOImpl = transactionAggregationDAOImpl;
        this.transactionAggregationUpdateLogServiceImpl = transactionAggregationUpdateLogServiceImpl;
    }

    @PostConstruct
    public void postConstruct() throws JsonParseException, JsonMappingException, IOException{
        populateTransactionAggregationMap();
    }

    private void populateTransactionAggregationMap() throws JsonParseException, JsonMappingException, IOException {
        synchronized(writelock){
            List<TransactionAggregation> transactions = transactionAggregationDAOImpl.readAll();   
            TreeMap<TransactionAggregation, BigDecimal> aggregationMap = new TreeMap<TransactionAggregation, BigDecimal>(TransactionAggregation::compareByDateThenType);
            
            for(TransactionAggregation tx : transactions){
                aggregationMap.put(tx, tx.getAmount());
            }
            transactionAggregations = aggregationMap;        
        }
    } 


    @Override
    public Optional<TransactionAggregation> getTransactionAggregation(LocalDate transactionDate, String transactionType) {
        TransactionAggregation aggregation = TransactionAggregation.builder().date(transactionDate).type(transactionType).build();
        
        BigDecimal aggregationAmt = transactionAggregations.get(aggregation);
        if(aggregationAmt != null){
            log.info("Transaction Type:{}, Amount:{}",transactionType,aggregationAmt);
            aggregation.setAmount(aggregationAmt);
            return Optional.of(aggregation);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void aggregateTransactions(List<TransactionAggregation> transactions) { 
        // writelock to control multithreaded access to the map updates and ensure we can rollback in event of failure
        synchronized(writelock){
            List<TransactionAggregationUpdateEvent> updateEvents = new ArrayList<>();
            Map<TransactionAggregation, BigDecimal> updateMap = new TreeMap<>((SortedMap<TransactionAggregation, BigDecimal>)transactionAggregations);

            for(TransactionAggregation tx : transactions){
                updateMap.merge(tx, tx.getAmount(), (old,toMerge) -> {
                    BigDecimal mergedValue = old.add(toMerge);
                    updateEvents.add(TransactionAggregationUpdateEvent.builder()
                                        .date(tx.getDate())
                                        .type(tx.getType())
                                        .beforeAmount(old)
                                        .afterAmount(mergedValue)
                                        .build());
                    return mergedValue;
                });
            }
            boolean saveSuccess = saveTransactionState(updateMap);
            if(saveSuccess){
                // Wait for save to be successful before updating the map for read requests
                transactionAggregations = updateMap;
                if(!updateEvents.isEmpty()){
                    transactionAggregationUpdateLogServiceImpl.recordAggregationUpdates(updateEvents);
                }
            }            
        }
    }

    private boolean saveTransactionState(Map<TransactionAggregation, BigDecimal> aggregationMap){
        List<TransactionAggregation> aggregations = aggregationMap.entrySet()
            .stream()
            .map(e -> TransactionAggregation.builder().date(e.getKey().getDate()).type(e.getKey().getType()).amount(e.getValue()).build())
            .collect(Collectors.toList());
        
        try{
            transactionAggregationDAOImpl.updateAll(aggregations);
            return true;
        } catch (Exception ex){
            return false;
        }        
    }

}
