package com.gavin.shane.transactionaggregatorsvc.service;

import java.util.List;

import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

public interface TransactionAggregationUpdateLogService {
    void recordAggregationUpdates(List<TransactionAggregationUpdateEvent> updateEvents);
}
