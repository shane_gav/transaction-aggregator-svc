package com.gavin.shane.transactionaggregatorsvc.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;

public interface TransactionAggregationService {
    Optional<TransactionAggregation> getTransactionAggregation(LocalDate transactionDate, String transactionType);

    void aggregateTransactions(List<TransactionAggregation> transactionAggregations);
}
