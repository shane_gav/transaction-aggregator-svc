package com.gavin.shane.transactionaggregatorsvc.service;

import java.util.List;


import com.gavin.shane.transactionaggregatorsvc.dao.TransactionAggregationUpdateEventDAO;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionAggregationUpdateLogServiceImpl implements TransactionAggregationUpdateLogService{

    TransactionAggregationUpdateEventDAO transactionAggregationUpdateEventDAOImpl;

    public TransactionAggregationUpdateLogServiceImpl(@Autowired TransactionAggregationUpdateEventDAO transactionAggregationUpdateEventDAOImpl){
        this.transactionAggregationUpdateEventDAOImpl = transactionAggregationUpdateEventDAOImpl;
    }

    @Override
    public void recordAggregationUpdates(List<TransactionAggregationUpdateEvent> updateEvents) {
       try{
            transactionAggregationUpdateEventDAOImpl.appendAll(updateEvents);
       } catch (Exception ex) {
           //Handle our retry logic or use something like resilience4j to implement backoff/retry
       }        
    }
        
}
