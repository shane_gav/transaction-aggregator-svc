package com.gavin.shane.transactionaggregatorsvc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import lombok.Data;

@ConfigurationProperties(prefix="com.gavin.shane.resource")
@Configuration
@Data
public class ResourceConfig{
    Resource transactionAggregationSource;
    Resource transactionAggregationUpdateLog;
}