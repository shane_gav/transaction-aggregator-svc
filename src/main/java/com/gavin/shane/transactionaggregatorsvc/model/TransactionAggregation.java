package com.gavin.shane.transactionaggregatorsvc.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.gavin.shane.transactionaggregatorsvc.validation.StringInListConstraint;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionAggregation{
    LocalDate date;
    BigDecimal amount;

    @StringInListConstraint(availableValues = {"debit", "credit"}, ignoreCase = true)
    String type;

    public static int compareByDateThenType(TransactionAggregation tx1, TransactionAggregation tx2){       
        int dateCompare = tx1.getDate().compareTo(tx2.getDate());
        if(dateCompare == 0){
            return tx1.getType().toLowerCase().compareTo(tx2.getType().toLowerCase());  
        }
        return dateCompare;
    }
}