package com.gavin.shane.transactionaggregatorsvc.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionAggregationUpdateEvent {
    LocalDate date;
    String type;
    BigDecimal beforeAmount;
    BigDecimal afterAmount;
}
