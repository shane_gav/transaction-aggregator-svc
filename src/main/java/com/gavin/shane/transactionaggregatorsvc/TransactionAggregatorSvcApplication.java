package com.gavin.shane.transactionaggregatorsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionAggregatorSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionAggregatorSvcApplication.class, args);
	}

}
