package com.gavin.shane.transactionaggregatorsvc.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.gavin.shane.transactionaggregatorsvc.api.TransactionAggregationsApi;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.gavin.shane.transactionaggregatorsvc.service.TransactionAggregationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionAggregationController implements TransactionAggregationsApi{

    TransactionAggregationService transactionAggregationServiceImpl;

    public TransactionAggregationController(@Autowired TransactionAggregationService transactionAggregationServiceImpl){
        this.transactionAggregationServiceImpl = transactionAggregationServiceImpl;
    }

    @Override
    public ResponseEntity<Void> aggregateTransaction(
            @Valid List<TransactionAggregation> transactionAggregation) { 
        transactionAggregationServiceImpl.aggregateTransactions(transactionAggregation);
        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<TransactionAggregation> getTransactions(
            @Valid @NotNull LocalDate date, @Valid @NotNull String type) {
        Optional<TransactionAggregation> aggregation = transactionAggregationServiceImpl.getTransactionAggregation(date, type);

        if(aggregation.isPresent()){
            return ResponseEntity.ok()
                .body(aggregation.get());
        } else {
            return ResponseEntity.notFound().build();
        }      
        
    }
    
}
