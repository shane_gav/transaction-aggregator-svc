package com.gavin.shane.transactionaggregatorsvc.validation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringInListValidator.class)
public @interface StringInListConstraint {

    String message() default "String must be part of available values";

    Class<?>[] groups() default {};

    Class<? extends Payload[]>[] payload() default {};

    String[] availableValues();

    boolean ignoreCase();

}
