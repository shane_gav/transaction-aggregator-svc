package com.gavin.shane.transactionaggregatorsvc.validation;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StringInListValidator implements ConstraintValidator<StringInListConstraint, String> {

    String[] validStrings;
    boolean ignoreCase;
    
    @Override
    public void initialize(StringInListConstraint constraint) {
        validStrings = constraint.availableValues();
        ignoreCase = constraint.ignoreCase();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(ignoreCase){
            return Arrays.stream(validStrings)
            .anyMatch(s -> s.toLowerCase().equals(value.toLowerCase()));
        } else {
            return Arrays.stream(validStrings)
            .anyMatch(s -> s.equals(value));
        }        
    }

}