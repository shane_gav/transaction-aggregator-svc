package com.gavin.shane.transactionaggregatorsvc.dao;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;

public interface TransactionAggregationDAO {
    List<TransactionAggregation> readAll() throws JsonParseException, JsonMappingException, IOException;
    void updateAll(List<TransactionAggregation> transactionAggregation) throws JsonGenerationException, JsonMappingException, IOException;
}
