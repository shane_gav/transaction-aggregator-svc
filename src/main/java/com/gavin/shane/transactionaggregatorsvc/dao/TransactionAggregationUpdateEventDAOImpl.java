package com.gavin.shane.transactionaggregatorsvc.dao;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gavin.shane.transactionaggregatorsvc.config.ResourceConfig;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TransactionAggregationUpdateEventDAOImpl implements TransactionAggregationUpdateEventDAO{

    private static final Object lock = new Object();

    private JavaType listType;
    private ObjectMapper objectMapper;
    private Resource transactionResource;


    public TransactionAggregationUpdateEventDAOImpl(@Autowired ObjectMapper objectMapper, @Autowired ResourceConfig resourceConfig){
        this.objectMapper = objectMapper;
        this.transactionResource = resourceConfig.getTransactionAggregationUpdateLog();
        this.listType = objectMapper.getTypeFactory().constructCollectionType(List.class, TransactionAggregationUpdateEvent.class);
    }

    @Override
    public void appendAll(List<TransactionAggregationUpdateEvent> newLogEvents)
            throws JsonGenerationException, JsonMappingException, IOException {
        synchronized(lock){
            
            List<TransactionAggregationUpdateEvent> existingRecords = new ArrayList<TransactionAggregationUpdateEvent>();
            if(transactionResource.exists()){
                try{
                    existingRecords.addAll(objectMapper.readValue(transactionResource.getFile(), listType)); 
                } catch (Exception ex) {
                    log.info("Json file records on system not parsing. Lets Delete file and start fresh.");
                    transactionResource.getFile().delete();
                }
            }

            existingRecords.addAll(newLogEvents);
            objectMapper.writeValue(transactionResource.getFile(), existingRecords);    
            
        }            
    }
    
}
