package com.gavin.shane.transactionaggregatorsvc.dao;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gavin.shane.transactionaggregatorsvc.config.ResourceConfig;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class TransactionAggregationDAOImpl implements TransactionAggregationDAO{

    private static final Object lock = new Object();

    private JavaType listType;
    private ObjectMapper objectMapper;
    private Resource transactionResource;


    public TransactionAggregationDAOImpl(@Autowired ObjectMapper objectMapper, @Autowired ResourceConfig resourceConfig){
        this.objectMapper = objectMapper;
        this.transactionResource = resourceConfig.getTransactionAggregationSource();
        this.listType = objectMapper.getTypeFactory().constructCollectionType(List.class, TransactionAggregation.class);
    }

    @Override
    public List<TransactionAggregation> readAll() throws JsonParseException, JsonMappingException, IOException {
        synchronized(lock){
            if(transactionResource.getFile().exists()){
                return objectMapper.readValue(transactionResource.getFile(), listType); 
            } else {
                return new ArrayList<TransactionAggregation>();
            } 
        }       
    }

    @Override
    public void updateAll(List<TransactionAggregation> transactionAggregation) throws JsonGenerationException, JsonMappingException, IOException {
        synchronized(lock){
            objectMapper.writeValue(transactionResource.getFile(), transactionAggregation);   
        }       
    }
    
}
