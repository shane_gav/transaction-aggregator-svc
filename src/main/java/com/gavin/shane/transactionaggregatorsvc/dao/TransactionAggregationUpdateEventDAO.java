package com.gavin.shane.transactionaggregatorsvc.dao;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

public interface TransactionAggregationUpdateEventDAO {
    void appendAll(List<TransactionAggregationUpdateEvent> transactionAggregation) throws JsonGenerationException, JsonMappingException, IOException;
}
