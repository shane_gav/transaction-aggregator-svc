package com.gavin.shane.transactionaggregatorsvc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;

public class CapacityGenerator {
    public static void main(String[] args) throws IOException{
        List<TransactionAggregation> tx = new ArrayList<TransactionAggregation>();
        
        List<TransactionAggregationUpdateEvent> updates = new ArrayList<TransactionAggregationUpdateEvent>();
        
        for(int j=2020; j<=2029; j++){
            for(int i=1; i<=365; i++){
            String type = (i%2 == 0 ? "debit" : "credit");

            BigDecimal amount1 = BigDecimal.valueOf(Math.random()).multiply(BigDecimal.valueOf(Long.MAX_VALUE));
            BigDecimal amount2 = amount1.multiply(BigDecimal.valueOf(2L));

            TransactionAggregation agg = TransactionAggregation.builder()
            .amount(amount2)
            .date(LocalDate.ofYearDay(j, i))
            .type(type)
            .build();

            tx.add(agg);

            updates.add(TransactionAggregationUpdateEvent.builder()
                .beforeAmount(amount1)
                .afterAmount(amount2)
                .date(LocalDate.ofYearDay(j, i))
                .type(type).build());
            }
        }
        ObjectMapper obj = new ObjectMapper().registerModule(new JavaTimeModule());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        obj.setDateFormat(df);

        //Standard objects
        try(OutputStream fso = new FileOutputStream(new File("C://dev//out.json"))){
            obj.writeValue(fso, tx);    
        }

        //Stanard log events
        try(OutputStream fso = new FileOutputStream(new File("C://dev//out-log.json"))){
            obj.writeValue(fso, updates);    
        }


        //Gzip objects
        try(OutputStream fso = new GZIPOutputStream(new FileOutputStream(new File("C://dev//out.json.gz")))){
            obj.writeValue(fso, tx);    
        }

        //GZIP log events
        try(OutputStream fso = new GZIPOutputStream(new FileOutputStream(new File("C://dev//out-log.json.gz")))){
            obj.writeValue(fso, updates);    
        }
    }
}
