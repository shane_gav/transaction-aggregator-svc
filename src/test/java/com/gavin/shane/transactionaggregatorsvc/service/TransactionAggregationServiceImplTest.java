package com.gavin.shane.transactionaggregatorsvc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gavin.shane.transactionaggregatorsvc.dao.TransactionAggregationDAO;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregationUpdateEvent;
import com.google.common.collect.Lists;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TransactionAggregationServiceImplTest {

    private static final String CREDIT = "credit";    
    private static final String DEBIT = "debit";    
    private static final BigDecimal yesterday_amt = BigDecimal.valueOf(50L);
    private static final BigDecimal today1_amt = BigDecimal.valueOf(70L);
    private static final BigDecimal today2_amt = BigDecimal.valueOf(10L);
    private static final LocalDate TEST_YESTERDAY = LocalDate.of(2021,01,01);
    private static final LocalDate TEST_TODAY = LocalDate.of(2021,01,02);
    private TransactionAggregation yesterday, today1, today2;

    @Mock 
    TransactionAggregationUpdateLogService transactionAggregationUpdateLogServiceImpl;
    @Mock 
    TransactionAggregationDAO transactionAggregationDAOImpl;

    @InjectMocks 
    TransactionAggregationServiceImpl transactionAggregationServiceImpl;

    @Captor
    ArgumentCaptor<List<TransactionAggregation>> updateValues;
    @Captor
    ArgumentCaptor<List<TransactionAggregationUpdateEvent>> updateEvtCaptor;

    @BeforeEach
    public void setup() throws Exception{
        //Setup initial empty map
        when(transactionAggregationDAOImpl.readAll())
            .thenReturn(new ArrayList<TransactionAggregation>());
      
        transactionAggregationServiceImpl.postConstruct();

        yesterday = TransactionAggregation.builder()
        .amount(yesterday_amt)
        .type(CREDIT)
        .date(TEST_YESTERDAY)
        .build();

        today1 = TransactionAggregation.builder()
        .amount(today1_amt)
        .type(CREDIT)
        .date(TEST_TODAY)
        .build();

        today2 = TransactionAggregation.builder()
        .amount(today2_amt)
        .type(CREDIT)
        .date(TEST_TODAY)
        .build();        
    }

    @Test
    public void givenATransactionIsAggregated_whenItIsNew_thenTheTransactionIsCreated() throws JsonParseException, JsonMappingException, IOException{
        // Arrange
        List<TransactionAggregation> toAggregate = Lists.newArrayList(today1);
      
        // Act
        Optional<TransactionAggregation> beforeTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	
        transactionAggregationServiceImpl.aggregateTransactions(toAggregate);
        Optional<TransactionAggregation> afterTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	

        // Assert
        assertFalse(beforeTransaction.isPresent());

        TransactionAggregation tx = afterTransaction.get();
        assertEquals(CREDIT, tx.getType());
        assertEquals(today1_amt, tx.getAmount());
        assertEquals(TEST_TODAY, tx.getDate());

        verify(transactionAggregationDAOImpl, times(1)).updateAll(toAggregate);
    }


    @Test
    public void givenATransactionIsAggregated_whenItAlreadyExists_thenTheTransactionAmountIsUpdated_andTheUpdateIsCapturedByTheLogService() throws JsonGenerationException, JsonMappingException, IOException{
        // Arrange      
        List<TransactionAggregation> toAggregate1 = Lists.newArrayList(today1);
        List<TransactionAggregation> toAggregate2 = Lists.newArrayList(today2);
        
        // Act
        Optional<TransactionAggregation> beforeTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	

        transactionAggregationServiceImpl.aggregateTransactions(toAggregate1);
        Optional<TransactionAggregation> afterTransactions1 = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	
        transactionAggregationServiceImpl.aggregateTransactions(toAggregate2);
        Optional<TransactionAggregation> afterTransactions2 = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	

        // Assert

        assertTrue(beforeTransaction.isEmpty());

        // Check the right values were returned after
        TransactionAggregation tx = afterTransactions1.get();
        assertEquals(CREDIT, tx.getType());
        assertEquals(today1_amt, tx.getAmount());
        assertEquals(TEST_TODAY, tx.getDate());

        TransactionAggregation tx1 = afterTransactions2.get();
        assertEquals(CREDIT, tx1.getType());
        assertEquals(today1_amt.add(today2_amt), tx1.getAmount());
        assertEquals(TEST_TODAY, tx1.getDate());

        // Check the right calls were made to the DAO
        verify(transactionAggregationDAOImpl, times(2)).updateAll(updateValues.capture());
        List<TransactionAggregation> daoUpdate1 = updateValues.getAllValues().get(0);
        assertEquals(1, daoUpdate1.size());
        assertEquals(today1_amt, daoUpdate1.get(0).getAmount());
        assertEquals(CREDIT, daoUpdate1.get(0).getType());
        assertEquals(TEST_TODAY, daoUpdate1.get(0).getDate());

        List<TransactionAggregation> daoUpdate2 = updateValues.getAllValues().get(1);
        assertEquals(1, daoUpdate2.size());
        assertEquals(today1_amt.add(today2_amt), daoUpdate2.get(0).getAmount());
        assertEquals(CREDIT, daoUpdate2.get(0).getType());
        assertEquals(TEST_TODAY, daoUpdate2.get(0).getDate());
        
        // Check the right calls were made to the logging service
        verify(transactionAggregationUpdateLogServiceImpl, times(1)).recordAggregationUpdates(updateEvtCaptor.capture());
        List<TransactionAggregationUpdateEvent> updateEvents1 = updateEvtCaptor.getAllValues().get(0);
        assertEquals(1,updateEvents1.size());
        assertEquals(TEST_TODAY, updateEvents1.get(0).getDate());
        assertEquals(CREDIT, updateEvents1.get(0).getType());
        assertEquals(today1_amt, updateEvents1.get(0).getBeforeAmount());
        assertEquals(today1_amt.add(today2_amt), updateEvents1.get(0).getAfterAmount());
    }

    
    @Test
    public void givenATransactionIsAggregated_whenItIsAddedTwiceInTheSameList_thenTheTransactionAmountIsAggregated_andTheUpdateIsCapturedByTheLogService() throws JsonGenerationException, JsonMappingException, IOException{
        // Arrange
        List<TransactionAggregation> toAggregate1 = Lists.newArrayList(today1, today2, today2);

        // Act
        Optional<TransactionAggregation> beforeTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	
        transactionAggregationServiceImpl.aggregateTransactions(toAggregate1);
        Optional<TransactionAggregation> afterTransactions = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	

        // Assert
        assertTrue(beforeTransaction.isEmpty());

        TransactionAggregation tx1 = afterTransactions.get();
        assertEquals(CREDIT, tx1.getType());
        assertEquals(today1_amt.add(today2_amt).add(today2_amt), tx1.getAmount());
        assertEquals(TEST_TODAY, tx1.getDate());

        // The code will just save once to disk to save rewrites
        verify(transactionAggregationDAOImpl, times(1)).updateAll(updateValues.capture());
        List<TransactionAggregation> daoUpdate1 = updateValues.getAllValues().get(0);
        assertEquals(1, daoUpdate1.size());
        assertEquals(today1_amt.add(today2_amt).add(today2_amt), daoUpdate1.get(0).getAmount());
        assertEquals(CREDIT, daoUpdate1.get(0).getType());
        assertEquals(TEST_TODAY, daoUpdate1.get(0).getDate());

        verify(transactionAggregationUpdateLogServiceImpl, times(1)).recordAggregationUpdates(updateEvtCaptor.capture());
        
        // We should get 2 log events as we are aggregating twice based on request input
        List<TransactionAggregationUpdateEvent> updateEvents1 = updateEvtCaptor.getAllValues().get(0);
        assertEquals(2,updateEvents1.size());
        assertEquals(TEST_TODAY, updateEvents1.get(0).getDate());
        assertEquals(CREDIT, updateEvents1.get(0).getType());
        assertEquals(today1_amt, updateEvents1.get(0).getBeforeAmount());
        assertEquals(today1_amt.add(today2_amt), updateEvents1.get(0).getAfterAmount());

        assertEquals(TEST_TODAY, updateEvents1.get(1).getDate());
        assertEquals(CREDIT, updateEvents1.get(1).getType());
        assertEquals(today1_amt.add(today2_amt), updateEvents1.get(1).getBeforeAmount());
        assertEquals(today1_amt.add(today2_amt).add(today2_amt), updateEvents1.get(1).getAfterAmount());
    }


    @Test
    public void givenATransactionIsAggregated_whenExeptionOccurs_thenTheMapRemainsTheSame() throws JsonParseException, JsonMappingException, IOException{
        // Arrange
        List<TransactionAggregation> toAggregate = Lists.newArrayList(today1);
      
        doThrow(new RuntimeException()).when(transactionAggregationDAOImpl).updateAll(any());
            
        // Act
        Optional<TransactionAggregation> beforeTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	
        transactionAggregationServiceImpl.aggregateTransactions(toAggregate);
        Optional<TransactionAggregation> afterTransaction = transactionAggregationServiceImpl.getTransactionAggregation(TEST_TODAY, CREDIT);	

        // Assert
        assertFalse(beforeTransaction.isPresent());
        assertFalse(afterTransaction.isPresent());
    }

}
