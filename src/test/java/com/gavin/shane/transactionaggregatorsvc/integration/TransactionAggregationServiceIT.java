package com.gavin.shane.transactionaggregatorsvc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.gavin.shane.transactionaggregatorsvc.config.ResourceConfig;
import com.gavin.shane.transactionaggregatorsvc.model.TransactionAggregation;
import com.google.common.collect.Lists;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, properties = {
    "com.gavin.shane.resource.transaction-aggregation-source=transaction-repo.json",
    "com.gavin.shane.resource.transaction-aggregation-update-log=update-log-repo.json"
})
@DirtiesContext
public class TransactionAggregationServiceIT {
    @Autowired
    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Autowired ResourceConfig resourceConfig;
    
    @BeforeAll
    public static void setupClass() throws IOException{
        URL f1 = TransactionAggregationServiceIT.class.getClassLoader().getResource("transaction-repo.json");
        if(f1 != null){
            Files.deleteIfExists(Path.of(f1.getFile()));
        }
        URL f2 = TransactionAggregationServiceIT.class.getClassLoader().getResource("update-log-repo.json");
        if(f2 != null){
            Files.deleteIfExists(Path.of(f2.getFile()));
        }
        
    }

    @BeforeEach
    public void setup() throws IOException{
        Files.deleteIfExists(resourceConfig.getTransactionAggregationSource().getFile().toPath());
        Files.deleteIfExists(resourceConfig.getTransactionAggregationUpdateLog().getFile().toPath());
    }

    @Test
    public void givenwebserviceIsAvailable_whenRecordDoesntExist_thenReturns404(){
        
        Map<String, String> params = new HashMap<String,String>();
        params.put("date", "2020-01-01");
        params.put("type", "credit");

        ResponseEntity<TransactionAggregation> response 
        = testRestTemplate.getForEntity("/transaction-aggregations?date={date}&type={type}", TransactionAggregation.class, params);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        
    }

    @Test
    public void givenwebserviceIsAvailable_whenIncorrectDate_thenReturns400(){
        
        Map<String, String> params = new HashMap<String,String>();
        params.put("date", "broken");
        params.put("type", "debit");

        ResponseEntity<TransactionAggregation> response 
        = testRestTemplate.getForEntity("/transaction-aggregations?date={date}&type={type}", TransactionAggregation.class, params);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        
    }
    @Test
    public void givenwebserviceIsAvailable_whenRequestIsMadeToAggregate_thenAggregationIsSaved(){
       

        LocalDate dt = LocalDate.of(2020,01,01);
        String type="debit";
        BigDecimal amount=BigDecimal.valueOf(50L);

        Map<String, Object> params = new HashMap<String,Object>();
        params.put("date", "2020-01-01");
        params.put("type", type);

        TransactionAggregation tx = TransactionAggregation.builder()
        .amount(amount)
        .date(dt)
        .type(type).build();

        ResponseEntity<TransactionAggregation> response1
        = testRestTemplate.getForEntity("/transaction-aggregations?date={date}&type={type}", TransactionAggregation.class, params);
        
        ResponseEntity<Void> response2 = testRestTemplate.postForEntity("/transaction-aggregations", 
        Lists.newArrayList(tx), Void.class);

        ResponseEntity<TransactionAggregation> response3
        = testRestTemplate.getForEntity("/transaction-aggregations?date={date}&type={type}", TransactionAggregation.class, params);

        
        assertEquals(HttpStatus.NOT_FOUND, response1.getStatusCode());
        assertEquals(HttpStatus.OK, response2.getStatusCode());
        assertEquals(HttpStatus.OK, response3.getStatusCode());

        assertEquals(amount, response3.getBody().getAmount());
        assertEquals(type, response3.getBody().getType());
        assertEquals(dt, response3.getBody().getDate());
        
    }


    @Test
    public void givenwebserviceIsAvailable_whenInvalidTypePosted_ThenReturn400(){
       

        LocalDate dt = LocalDate.of(2020,01,01);
        String type="BROKEN";
        BigDecimal amount=BigDecimal.valueOf(50L);

        TransactionAggregation tx = TransactionAggregation.builder()
        .amount(amount)
        .date(dt)
        .type(type).build();
        
        ResponseEntity<Void> response = testRestTemplate.postForEntity("/transaction-aggregations", 
        Lists.newArrayList(tx), Void.class);
        
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());        
    }

}
